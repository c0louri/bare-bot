import sys
import re

rgx = "Passed (\d+)/(\d+) test stories"

results_file = sys.argv[1]

with open(results_file, 'r') as r_f:
    lines = r_f.readlines()
    last_line = lines[-1]
    vals = re.findall(rgx, last_line)
    if vals == []:
        print("Error with parsing e2e_test results")
        exit(1)
    succ, total = int(vals[0][0]), int(vals[0][1])
    per = succ / total
    if per == 1.0:
        print("Passed successfully all ({}) test stories".format(total))
        exit(0)
    if per >= 0.95:
        # there have been some errors
        print(''.join(lines))
        print("Acceptable percentage of failures")
        exit(0)
    else:
        # percentage of errors is not acceptable
        print(''.join(lines))
        print("Percentage of errors is not acceptable!")
        exit(1)
