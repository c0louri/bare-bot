import requests
import sys
import re
import json
from urllib import parse
import uuid
import logging
import io

DEBUG=False

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

RASA_TIMEOUT=30
RASA_ENDPOINT="webhooks/rest/webhook"

def remove_extra_spaces(inputstring):
    res = re.sub(r"\s+"," ",inputstring)
    return res

def remove_newline_and_indentation_before_bar(instr):
    res= re.sub(r"\n[ \t]*\|","|",instr)
    return res

class TinyClient:

    def __init__(self,host="localhost",port="5005"):
        self.host = host
        self.port = port
        self.uuid = uuid.uuid1()

    def tell_rasa(self, msg):
        """Sends a text message to rasa and gets the response.

        Sends an HTTP GET request to Rasa and waits asynchronously for the response. 
        The response is a json string and contains various responses, having 
        a ``text`` field, which is what this function returns. In case of empty text
        fields, an error message is returned to the client.

        Args:
            msg (str) : the text message of the user
        Returns:
            (str) : rasa's response to the user 
        """

        try:
            all_responses=""

            logging.debug(str(self.uuid) + " says '" + 
                         msg + "'. Forwarding to Rasa.")

            requestEndpoint="http://" + self.host+':' + self.port+"/" + RASA_ENDPOINT

            requestBody={'message': msg, 'sender': str(self.uuid)}
            requestBody_json=json.loads(json.dumps(requestBody))
            # adding exceptions for bad statuses 
            # from https://stackoverflow.com/a/16511493
            responseString = requests.post(url = requestEndpoint, json = requestBody_json)
            responseString.raise_for_status()

            appended_responses = [x['text'] + " " for x in responseString.json()]
            all_responses = ''.join(appended_responses)


            if all_responses is None:
                logging.error(
                    "Got None message from rasa. This should not happen.")
                all_responses = "An error has occured, please close and open the application."
            elif all_responses == "":
                logging.error("Rasa replied an empty string")
                all_responses = "I'm sorry, something went wrong, try again!"
            else:
                logging.debug("Got message from Rasa '" + all_responses + "'")

        except requests.exceptions.ConnectionError as e:
                # HTTPError is raised for non-200 responses; the response
                # can be found in e.response.

            logging.error(str(e))
            all_responses = "The chatbot couldnt be reached, are you sure it is running?"

        return all_responses


    

    def test_conversation(self,story,storyname,debug):

        print("*"*40)
        print(f"{bcolors.HEADER}Running test for: " + storyname + f"{bcolors.ENDC}")
        pos = story.find("_BOT: ")
        while(pos != -1):
            user_msg = story[7:pos].strip()
            print("user says: " + user_msg)
            story = story[pos:]
            pos = story.find("_USER: ")
            rasa_desired_response = remove_extra_spaces(remove_newline_and_indentation_before_bar(story[6:pos].strip()))
            rasa_actual_response = remove_extra_spaces(self.tell_rasa(user_msg).strip())
            print("bot says: " + rasa_actual_response)
            if debug:
                print("Checking if rasa response matches with:")
                print(rasa_desired_response)
            if not(re.search(rasa_desired_response,rasa_actual_response)):
                print(f"{bcolors.FAIL}Story failed at : " + user_msg + f"{bcolors.ENDC}")
                print("Expected '" + rasa_desired_response + "' but got '" + rasa_actual_response + "'")
                print("*"*40)
                return False
            story = story[pos:]
        
            pos = story.find("_BOT: ")

#        print(f."{bcolors.OKGREEN} Story {} passed the test!{bcolors.ENDC}".format(storyname))
        print(f"{bcolors.OKGREEN} Story passed the test!{bcolors.ENDC}")
        print("*"*40)

        return True

    
passed_stories=0
total_stories=0
for arg in sys.argv[1:]:
    if arg=="--debug":
        DEBUG=True
    else:
        total_stories=total_stories+1

for storyfile in sys.argv[1:]:
    if storyfile=="--debug": 
        continue
    tc = TinyClient()
    with io.open(storyfile,'r',encoding= "utf-8") as f:
        story = f.read()
    passed = tc.test_conversation(story,storyfile,DEBUG)
    if passed:
        passed_stories = passed_stories + 1 

print("*"*40)
print("Passed {}/{} test stories".format(passed_stories,total_stories))

