#!/usr/bin/bash

# TODO: instead of sleep for 60 secs, add code to check when rasa servers are online and ready
rasa run &
RASA_PID=$!
rasa run actions &
RASA_ACT_PID=$!
cd rasa_e2e_test
sleep 60
python rasa_e2e_test.py *.txt > e2e.out
kill -9 $RASA_ACT_PID
kill -9 $RASA_PID



